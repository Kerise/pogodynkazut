<?php

namespace App\Entity;

use App\Repository\MeasurementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MeasurementRepository::class)
 */
class Measurement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Location::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $location_id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=0)
     */
    private $temperature;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $recomendation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocationId(): ?Location
    {
        return $this->location_id;
    }

    public function setLocationId(?Location $location_id): self
    {
        $this->location_id = $location_id;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTemperature(): ?string
    {
        return $this->temperature;
    }

    public function setTemperature(string $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getPix(): ?string
    {
        return $this->pix;
    }

    public function setPix(string $pix): self
    {
        $this->pix = $pix;

        return $this;
    }

    public function getRecomendation(): ?string
    {
        return $this->recomendation;
    }

    public function setRecomendation(string $recomendation): self
    {
        $this->recomendation = $recomendation;

        return $this;
    }
}
